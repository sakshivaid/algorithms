#include<bits/stdc++.h>
using namespace std;

class node{
    public:
        node *left,*right;
        char ch;
        string s;
    node(){
        s = "";
        left = NULL;
        right = NULL;
    }
};

int main(){
    cout<<"enter the no.of characters"<<endl;
    int n; //no.of frequencies
    cin>>n;
    priority_queue<pair<int,node*>,vector<pair<int,node*> >,greater<pair<int,node*> > >q; //min heap
    for(int i = 0; i < n; i++){
        cout<<"enter each character and frequency"<<endl;
        int freq;
        char ch;
        cin>>ch>>freq;
        node *g;
        g=new(node);
        g->ch=ch;
        q.push(make_pair(freq,g));
    }
    while(q.size()!=1){
        node *g;
        g=new(node);
        int freq1=q.top().first;
        g->left=q.top().second;
        q.pop();
        int freq2=q.top().first;
        g->right=q.top().second;
        q.pop();
        q.push(make_pair(freq1+freq2,g));
    }
    queue<node*>p;
    p.push(q.top().second);
    while(!p.empty()){
        if(p.front()->left==NULL && p.front()->right==NULL)
            cout<<p.front()->s<<" "<<p.front()->ch<<endl;
        else{
            p.front()->left->s=p.front()->s+'0';
            p.front()->right->s=p.front()->s+'1';
            p.push(p.front()->left);
            p.push(p.front()->right);
        }
        p.pop();
    }
    return 0;
}


