/* Implementing 0-1 Knapsack */
#include <iostream>
using namespace std;

int Findmax(int a, int b) {
         return (a > b)? a : b;
}
int knapSack(int W, int weight[], int value[], int n) {

   if(n == 0 || W == 0)
       return 0;

   if(weight[n-1] > W)
       return knapSack(W, weight, value, n-1);

   else return Findmax(value[n-1] + knapSack(W - weight[n-1], weight, value, n-1),
                    knapSack(W, weight, value, n-1));
}

int main() {
    
	int n, W;

	cout<<"Enter the size of the sack"<<endl;
	cin>>W;
	cout<<"Enter number of items"<<endl;
	cin>>n;

	int weight[n];
	int value[n];
	cout<<"Enter weights"<<endl;
	for(int i = 0; i < n; i++) {
		cin>>weight[i];
	}

	cout<<"Enter corresponding values"<<endl;
	for(int i = 0; i < n; i++) {
                cin>>value[i];
        }

	
    	cout<<knapSack(W, weight, value, n);
	return 0;
}

