/* strongly connected components */

#include <bits/stdc++.h>
#include <fstream>
using namespace std;


int main(){

    ifstream ffile;

    ffile.open("strongly.txt", ios::in);
    int ver,edg;
    ffile>>ver>>edg;

    vector<int>v[ver+1];

      while(edg--){

        int x,y;
        ffile>>x>>y;
        v[x].push_back(y);
    }

    stack<int>s;

    int visited[ver+1];
    memset(visited, 0, sizeof visited);

    for(int i = 1;i <= ver; i++) {

        if(visited[i] == 0) {

            visited[i] = 1;
            stack<int>t;
            t.push(i);

            while(!t.empty()) {
                int node = t.top();
                for(int j = 0;j < v[node].size(); j++) {

                    if(visited[v[node][j]] == 0) {

                        t.push(v[node][j]);
                        visited[v[node][j]] = 1;
                        break;
                    }
                }
                if(t.top() == node){

                    s.push(t.top());
                    t.pop();
                }
            }
        }
    }
    vector<int>g[ver+1];

    for(int i = 1;i <= ver; i++) {

        for(int j = 0;j < v[i].size(); j++) {

            int temp = v[i][j];
            g[temp].push_back(i);
        }
    }
    memset(visited, 0, sizeof visited);

    stack<int>t;
    while(!s.empty()) {
        int node = s.top();
        s.pop();
        if(visited[node] == 0) {

            cout<<"strongly connected component/s is/are ";
            visited[node] = 1;
            cout<<node<<" ";

            t.push(node);

            while(!t.empty()) {
                node = t.top();
                for(int i = 0;i < g[node].size(); i++){
                    if(visited[g[node][i]] == 0){

                        t.push(g[node][i]);
                        visited[g[node][i]] = 1;
                        cout<<g[node][i]<<" ";
                        break;
                    }
                }
                if(t.top() == node){
                    t.pop();
                }
            }
            cout<<endl;
        }
    }
    return 0;
}
