/* krushkal's Algorithm */
#include <iostream>
#include <string.h>
#include <stdlib.h>
#include <fstream>
using namespace std;

struct Edge
{
    int src, dest, weight;
};

struct Graph
{

    int V, E;

     Edge* edge;
};

 Graph* createGraph(int V, int E)
{
    Graph* graph = new Graph;
    graph->V = V;
    graph->E = E;

    graph->edge = new Edge [graph->E ];
    return graph;
}


struct subset
{
    int parent;
    int rank;
};


int find(subset subsets[], int i)
{

    if (subsets[i].parent != i)
        subsets[i].parent = find(subsets, subsets[i].parent);

    return subsets[i].parent;
}


void Union( subset subsets[], int x, int y)
{
    int xroot = find(subsets, x);
    int yroot = find(subsets, y);

    if (subsets[xroot].rank < subsets[yroot].rank)
        subsets[xroot].parent = yroot;
    else if (subsets[xroot].rank > subsets[yroot].rank)
        subsets[yroot].parent = xroot;


    else
    {
        subsets[yroot].parent = xroot;
        subsets[xroot].rank++;
    }
}


int myComp(const void* a, const void* b)
{
    struct Edge* a1 = (struct Edge*)a;
    struct Edge* b1 = (struct Edge*)b;
    return a1->weight > b1->weight;
}


void KruskalMST(Graph* graph)
{
    int V = graph->V;
    struct Edge result[V];
    int e = 0;
    int i = 0;

    qsort(graph->edge, graph->E, sizeof(graph->edge[0]), myComp);


    subset *subsets = new subset[V];



    for (int v = 0; v < V; ++v)
    {
        subsets[v].parent = v;
        subsets[v].rank = 0;
    }


    while (e < V - 1)
    {

     Edge next_edge = graph->edge[i++];

        int x = find(subsets, next_edge.src);
        int y = find(subsets, next_edge.dest);

        if (x != y)
        {
            result[e++] = next_edge;
            Union(subsets, x, y);
        }

    }

    cout<<"MST"<<endl;
    for (i = 0; i < e; ++i)
        cout<<result[i].src<<"\t"<<result[i].dest<<"\tweight "<< result[i].weight<<endl;
    return;
}


int main()
{

    int V = 4;  // Number of vertices in graph
    int E = 5;  // Number of edges in graph
   Graph* graph = createGraph(V, E);
    ifstream ffile;
    ffile.open("krushkal.txt", ios::in);
    ffile>>V;
    ffile>>E;
    int a, b, w;
    for(int i = 0 ; i < E ;i++){
        ffile >> a >> b >>w;
        graph->edge[i].src = a;
        graph->edge[i].dest = b;
        graph->edge[i].weight = w;

    }

    KruskalMST(graph);

    return 0;
}
