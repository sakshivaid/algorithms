/* Matrix Chain Product */
#include <iostream>
#include <vector>
#include <limits.h>
#include <fstream>
using namespace std;

int matrix_chain (vector<int> p, int n) {
    int q, j;
    int arr[n][n];

    for (int i = 1; i < n; i++) {

        arr[i][i] = 0;

    }

    for (int b = 2; b < n; b++) {

        for (int i = 1; i < n-b+1; i++) {
            j = i + b - 1;
            arr[i][j] = INT_MAX;

            for(int k = i; k <= j-1; k++) {
                q = arr[i][k] + arr[k+1][j] + p[i-1]*p[k]*p[j];
                if (q < arr[i][j])
                    arr[i][j] = q;
            }
        }

    }

    return arr[1][n-1];
}

int main() {
    ifstream ffile;
    int num;
    vector<int> p;

    ffile.open("matrix.txt", ios::in);

    while(ffile >> num){
        p.push_back(num);
        ffile.get();

    }
    ffile.close();


    int size_arr = p.size();

    cout<<"Minimum number of multiplications is "<<matrix_chain(p, size_arr)<<endl;

   return 0;
}
