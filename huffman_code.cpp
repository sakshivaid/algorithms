#include <bits/stdc++.h>
#include <fstream>
using namespace std;

class node{
    public:
        node *left,*right;
        char ch;
        string s;

        node(){
            s = "";
            left = NULL;
            right = NULL;
        }
};

int main(){

    ifstream ffile;

    ffile.open("huffman.txt", ios::in);
    int n;
    ffile>>n;
    priority_queue<pair<int,node*>,vector<pair<int,node*> >,greater<pair<int,node*> > >q;

    for(int i = 0; i < n; i++){


        int freq;
        char ch;
        ffile>>ch>>freq;
        node *a;
        a = new(node);
        a->ch = ch;
        q.push(make_pair(freq, a));
    }

    while(q.size()!=1){

        node *a;
        a = new(node);
        int freq1 = q.top().first;
        a->left = q.top().second;
        q.pop();
        int freq2 = q.top().first;
        a->right = q.top().second;
        q.pop();
        q.push(make_pair(freq1 + freq2, a));
    }

    queue<node*>p;
    p.push(q.top().second);

    while(!p.empty()){
        if(p.front()->left==NULL && p.front()->right==NULL)
            cout<<p.front()->s<<" "<<p.front()->ch<<endl;
        else{
            p.front()->left->s=p.front()->s+'0';
            p.front()->right->s=p.front()->s+'1';
            p.push(p.front()->left);
            p.push(p.front()->right);
        }
        p.pop();
    }
    return 0;
}

